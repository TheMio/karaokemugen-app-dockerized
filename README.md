# karaokemugen-app-dockerized

Run a headless instance of karaoke mugen in docker, accessible over the webbrowser. Can be used as a playlist manager, but can't actually play karas. Why then? Well... just because it's possible. And because I like to manage my playlists on the go.


# Setting up
1. Clone this repo
2. Set the fields `INITIAL_USER_NAME` and `INITIAL_USER_PASSWORD` for your new admin user. The password must be atleast 8 or more characters.
3. Start the continer with `docker compose up -d`
4. Navigate to http://localhost:1337/setup and login (offline account) to setup the instance

Most of the errors in the console output can be ignored since there is no display to show the mpv player. After pulling and starting the container with `docker compose up` you can access the web interface on http://localhost:1337/


# Karaoke Mugen

KaraokeMugen source repository: https://gitlab.com/karaokemugen/karaokemugen-app 

KaraokeMugen webpage: https://mugen.karaokes.moe/
