#!/bin/bash

echo "Fixing permissions"

# Optionally give permissions to all folders
sudo chown 1000:1000 -R /data/KaraokeMugen/
sudo chmod 775 -R /data/KaraokeMugen/

# Specifically postgres needs 750 permissions
sudo chown 1000:1000 -R /data/KaraokeMugen/db/postgres
sudo chmod 750 -R /data/KaraokeMugen/db/postgres


echo "Starting KaraokeMugen"
export DISPLAY=':99.0'
Xvfb :99 -screen 0 1920x1080x24 > /dev/null 2>&1 &
sleep 1
"/opt/Karaoke Mugen/karaokemugen" --no-sandbox