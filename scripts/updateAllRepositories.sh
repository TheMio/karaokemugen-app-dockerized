#!/bin/bash
if [[ "$REPO_AUTO_UPDATE" == "true" ]]; then
        if [ -z "$KM_AUTHORIZATION_KEY" ]; then echo "KM authorization key was not provided as env var. Automatic repository update failed" ; fi
        curl --header "Content-Type: application/json" \
          --request POST   --data '{"body": {"authorization":"'"$KM_AUTHORIZATION_KEY"'"}, "cmd": "updateAllRepos"}' \
          http://localhost:1337/api/command
fi
