#!/bin/bash
wget https://mugen.karaokes.moe/downloads/latest-linux.yml -O "version.yml"
VERSION=$(cat version.yml | grep version | tr " " "\n" | sed -n '2p')
echo "$VERSION"
wget "https://mugen.karaokes.moe/downloads/Karaoke%20Mugen-${VERSION}-linux-amd64.deb" -O karaokemugen-app_amd64.deb
#     https://mugen.karaokes.moe/downloads/Karaoke%20Mugen-7.0.46-master-linux-amd64.deb
