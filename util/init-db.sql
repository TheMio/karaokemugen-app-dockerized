--CREATE DATABASE karaokemugen_server ENCODING 'UTF8';
--CREATE USER karaokemugen_server WITH ENCRYPTED PASSWORD 'musubi';
GRANT ALL PRIVILEGES ON DATABASE karaokemugen_app TO karaokemugen_app;

\c karaokemugen_app
CREATE EXTENSION unaccent;
CREATE EXTENSION pgcrypto;

-- Prevent postgres errors for missing role
CREATE USER postgres WITH SUPERUSER PASSWORD 'password';