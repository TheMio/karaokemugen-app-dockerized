FROM debian:bookworm
WORKDIR /opt/karaokemugen

RUN apt-get update && apt-get install -y xvfb chromium-shell patch git wget procps postgresql-client curl
# Set locales, no ones are set by default but required by postgres
RUN apt-get install -y locales
RUN echo en_US.UTF-8 UTF-8 > /etc/locale.gen
RUN locale-gen en_US.UTF-8

COPY scripts/downloadLatestKaraokeMugen.sh ./
RUN ./downloadLatestKaraokeMugen.sh
RUN ls
RUN chmod +x karaokemugen-app_amd64.deb && \
    apt install -y ./karaokemugen-app_amd64.deb

# Postgress can't run as root user, create a standard user
RUN mkdir /data && useradd -s /bin/bash -d /data dockeruser && chown -R dockeruser:dockeruser /data
RUN chown -R dockeruser:dockeruser /opt/karaokemugen/

# Allow user to perform sudo commands. Needed for setting correct file permissions
RUN apt install sudo
RUN adduser dockeruser sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> \
/etc/sudoers

# Copy over start script
COPY scripts/startKaraokeMugenHeadlessDocker.sh ./
EXPOSE 1337
RUN ["chmod", "+x", "/opt/karaokemugen/startKaraokeMugenHeadlessDocker.sh"]

# Copy repo autoupdate script
COPY scripts/updateAllRepositories.sh /etc/cron.daily/
RUN ["chmod", "+x", "/etc/cron.daily/updateAllRepositories.sh"]

USER dockeruser
ENTRYPOINT ["/opt/karaokemugen/startKaraokeMugenHeadlessDocker.sh"]
